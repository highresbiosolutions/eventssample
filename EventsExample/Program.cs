﻿using System;
using System.Linq;
using HRB.Cellario.Services.Client;
using HRB.Cellario.Services.DTO;
using HRB.Services.Client.Notifications;

namespace EventsExample
{
    public class Program
    {
        private ICellarioNotifications _notifications;
        static void Main(string[] args)
        {
            var program  = new Program();
            program.Start();
            Console.ReadLine();
        }

        public void Start()
        {
            var myIpAddress = "localhost";
            var myPort = 8555;
            _notifications = Startup.InitializeNotificationListener(myIpAddress, myPort);
            _notifications.OnSystemStateChanged += (sender, args) => Console.WriteLine($"System State Changed: {args.Event.State}");
            _notifications.OnSampleOperationStatusChanged += (sender, args) => Console.WriteLine($"Operation State Changed:{args.Event.Barcode} {args.Event.Name} {args.Event.State}");
            _notifications.OnOrderStateChanged += (sender, args) => Console.WriteLine($"Order State Changed: {args.Event.State}");
            var client = new CellarioClient("localhost", 8444);
            var subscribers = client.GetSubscribers();
            if (subscribers.FirstOrDefault(s => s.User == "EventsExample") != null)
                return;
            var subscriber = client.CreateSubscriber(new Subscriber()
            {
                User = "EventsExample",
                Url = $"http://localhost:8555/notifications",
                Filters = new Filter[]
                {
                    new Filter(EventType.System)
                    //new Filter(EventType.Operation)
                    //{
                    //    Operation = "LiquidTransfer",
                    //}
                }
            });

            Console.WriteLine($"Created Subscriber {subscriber.Id} - {subscriber.User}");
            Console.WriteLine("Run Cellario!");
        }
    }
}
